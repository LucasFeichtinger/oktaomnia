import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { HttpService } from 'src/app/services/http.service';
import { Bewertung } from 'src/app/models/bewertung';
import { DefaultRouteReuseStrategy } from '@angular/router/src/route_reuse_strategy';
import { _getComponentHostLElementNode } from '@angular/core/src/render3/instructions';

@Component({
    selector: 'app-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

    durchschnitt:any;
    bewertungen:any;
    user;

    constructor(private route: ActivatedRoute, private http: HttpService) {

    }

    ngOnInit() {
        this.http.data.loading = true;
        this.route.data.subscribe((data: { bewertungen, user }) => {
            this.user = data.user.user
            this.bewertungen = data.user.bewertung
            this.bewertungsdurchschnitt();
            this.http.data.loading = false;
            console.log(this.bewertungen.length);
        })
    }

    arrayOne(n: any): any[] {
        return Array(parseInt(n));
    }

    bewertungsdurchschnitt() {
        var num:number = 0;
        var count:number = this.bewertungen.length;
        for(var b of this.bewertungen) {
            num = num + b.starrating;
        }
        this.durchschnitt = num/count;
        this.durchschnitt = this.durchschnitt.toFixed(1);
        return this.durchschnitt;
    }
}
