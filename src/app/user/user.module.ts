import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../core/auth.guard';
import { UserDetailResolverService } from './user-detail-resolver.service';
import { DienstleistungenModule } from '../dienstleistungen/dienstleistungen.module';

const routes: Routes = [
    {
        path: 'user/:username',
        component: UserDetailComponent,
        canActivate: [AuthGuard],
        resolve: {
            user: UserDetailResolverService
        }
    }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    DienstleistungenModule
  ],
  declarations: [UserDetailComponent]
})
export class UserModule { }
