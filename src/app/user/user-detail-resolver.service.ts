import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { User } from '../models/user';
import { Observable, of, EMPTY } from 'rxjs';
import { HttpService } from '../services/http.service';
import { take, mergeMap } from 'rxjs/operators';
import { Dienstleistung } from '../models/dienstleistung';

@Injectable({
  providedIn: 'root'
})
export class UserDetailResolverService implements Resolve<Object> {

  constructor(private http:HttpService, private router:Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Object> | Observable<never> {
    this.http.data.loading = true;
    let username = route.paramMap.get('username');

    return this.http.getUser(username).pipe(
        take(1),
        mergeMap(user => {
            if (user) {
                this.http.data.loading = false;
                return this.http.loadBewertungenByUser(user).pipe(
                    take(1),
                    mergeMap(bewertung => {
                        console.log(bewertung)
                        this.http.data.loading = false;
                            return of( { bewertung, user })
                    })
                )
            } else {
                this.http.data.loading = false;
                this.router.navigate(['/dienstleistungen'])
                return EMPTY
            }
        })
    )
  }
}
