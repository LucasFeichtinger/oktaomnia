import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DienstleistungenComponent } from './dienstleistungen/dienstleistungen.component';
import { HttpClient, HttpHandler, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DienstleistungenModule } from './dienstleistungen/dienstleistungen.module';
import { AnfragenModule } from './anfragen/anfragen.module';
import { UserFormsModule } from './user-forms/user-forms.module';
import { UserModule } from './user/user.module';
import { ChatModule } from './chat/chat.module';
import { AccountModule } from './account/account.module';
import { AuthInterceptor } from './services/auth-interceptor';
import { RadarModule } from './radar/radar.module';
import { ToastrModule } from 'ng6-toastr-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WebSocketService } from './services/web-socket.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AnfragenModule,
    UserFormsModule,
    UserModule,
    ChatModule,
    AccountModule,
    RadarModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}, WebSocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
