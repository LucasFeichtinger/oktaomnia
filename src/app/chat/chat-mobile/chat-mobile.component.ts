import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { ChatService } from 'src/app/services/chat.service';
import { element } from '@angular/core/src/render3/instructions';
import { map } from 'rxjs/operators';
import { Message, Chat } from '../chat-list/chat-list.component';
import { WebSocketService } from 'src/app/services/web-socket.service';

@Component({
  selector: 'app-chat-mobile',
  templateUrl: './chat-mobile.component.html',
  styleUrls: ['./chat-mobile.component.css']
})
export class ChatMobileComponent implements OnInit {

    messageCol: AngularFirestoreCollection<Message> = new AngularFirestoreCollection<Message>(null, null, null);
    messages: Observable<Message[]>;

    cookieUsername: string;

    message: string;
    currentChatPartner: string;

    chat;
    chatId;

  constructor(private route: ActivatedRoute, public cs: ChatService, private socket:WebSocketService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
        this.chatId = params.get('id')
        if(this.chatId.length === 20) {
            this.getMessageToChat(this.chatId)

            this.cs.get(this.chatId).subscribe(data => {
                if (data.data.user1 === this.cs.http.auth.userLoggedin.username) {
                    this.currentChatPartner = data.data.user2;
                } else {
                    this.currentChatPartner = data.data.user1;
                }
            })
        } else {
            this.cs.http.getUserById(this.chatId).subscribe(data => {
                this.currentChatPartner = data.username;
                this.cs.afs.collection<Chat>('chats', ref => ref
                    .where('user1', '==', this.cs.http.auth.userLoggedin.username)
                    .where('user2', '==', data.username))
                    .snapshotChanges().subscribe(chats => {

                    this.cs.afs.collection<Chat>('chats', ref => ref
                        .where('user2', '==', this.cs.http.auth.userLoggedin.username)
                        .where('user1', '==', data.username))
                        .snapshotChanges().subscribe(cs => {
                        chats.map(doc => {
                            let data = doc.payload.doc.data() as Chat;
                            data.id = doc.payload.doc.id;
                            data.currentUser = this.cs.http.auth.userLoggedin.username;
                            return data
                        })
                        cs.map(doc => {
                            let data = doc.payload.doc.data() as Chat;
                            data.id = doc.payload.doc.id;
                            data.currentUser = this.cs.http.auth.userLoggedin.username;
                            return data
                        })
                        chats.concat(cs).forEach(data => {
                            if (data.payload.doc.id) {
                                this.getMessageToChat(data.payload.doc.id)
                            } else {
                                this.cs.createChat(this.cs.http.auth.userLoggedin, this.currentChatPartner).then(() => {
                                    this.getMessageToChat(data.payload.doc.id)
                                })
                            }
                        })
                    })

                })
            })
        }
        });
  }

  getMessageToChat(chatId) {
    this.messages = null
    if (chatId !== undefined) {
        this.messageCol = this.cs.afs.collection('chats').doc(chatId).collection('messages');
        this.messages = this.messageCol.valueChanges();
        this.scrollBot()
        this.sortMessagesByDate()
    }
  }

  sortMessagesByDate() {
        this.messages = this.messages.pipe(map((data) => {
        if (data !== null || data !== undefined) {
            data.sort((a, b) => {
                a.msg.replace(/(.{25})/g, "$1\A");
                a.sendetAt = new Date(a.sendetAt).toString();
                b.sendetAt = new Date(b.sendetAt).toString();
                if (new Date(a.sendetAt) > new Date(b.sendetAt)) return 1;
                else if (new Date(b.sendetAt) > new Date(a.sendetAt)) return -1;
                return 0;
            });
        }
      return data;
    }));
  }

  sendMessage() {
      if (this.cs.http.auth.userLoggedin) {
        const sendMess = this.message
        this.message = null;
        const ref = this.cs.afs.collection('chats').doc(this.chatId).collection('messages');
        this.scrollBot();
        this.socket.emitOnMessageSent(this.message, this.currentChatPartner, 'message');
        return ref.add({ msg: sendMess, sendetAt: Date.now(), sender: this.cs.http.auth.userLoggedin.username })
    }
  }

  formatDate(date:string) {
    var xd = new Date(date).toString();
    xd = xd.split(" ")[4].split(":")[0] + ":" + xd.split(" ")[4].split(":")[1];
    return xd;
    }

    scrollBot() {
        //$('#scrollbot').scrollBot($('#scrollbot')[0].scrollHeight);
        /*
        $('#scrollbot').stop().animate({
          scrollTop: $('#scrollbot')[0].scrollHeight
        }, 800);
        */
       var myVar = window.setInterval(function() {
            var elem = document.getElementById('scrollbot');
            elem.scrollTop = elem.scrollHeight;
       }, 1)

       setTimeout(() => {
        clearInterval(myVar)
       }, 1000)
      }

}
