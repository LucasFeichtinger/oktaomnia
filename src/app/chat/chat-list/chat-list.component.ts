import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable, of } from 'rxjs';
import { map, combineLatest } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
declare var $: any;

export interface Chat {
    id;
    user1:string;
    user2:string;
    currentUser:string;
}

export interface Message {
    msg:string;
    sendetAt:string;
    sender:string;
}

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.css']
})
export class ChatListComponent implements OnInit {

    mobile: boolean = navigator.userAgent.includes('Mobile');

    chatColUser1:AngularFirestoreCollection<Chat> = new AngularFirestoreCollection<Chat>(null, null, null);
    chatColUser2:AngularFirestoreCollection<Chat> = new AngularFirestoreCollection<Chat>(null, null, null);
    chats;
    chatUser1;
    chatUser2;

    selectedChat = null;

    receiver;

  constructor(private cs:ChatService, private route:ActivatedRoute) { }

  ngOnInit() {

      this.route.data.subscribe((data: { user, }) => {
        this.chatColUser1 = this.cs.afs.collection<Chat>('chats', ref => ref.where("user1", "==", this.cs.http.auth.userLoggedin.username));
        this.chatColUser2 = this.cs.afs.collection<Chat>('chats', ref => ref.where("user2", "==", this.cs.http.auth.userLoggedin.username));

       // this.chatUser1 = this.chatColUser1.valueChanges();
       // this.chatUser2 = this.chatColUser2.valueChanges();


        this.chatUser1 = this.chatColUser1.snapshotChanges().pipe(map(docs => {
            return docs.map(doc => {
              let data = doc.payload.doc.data() as Chat;
              data.id = doc.payload.doc.id;
              data.currentUser = this.cs.http.auth.userLoggedin.username;
              return data;
            })
        }))
        this.chatUser2 = this.chatColUser2.snapshotChanges().pipe(map(docs => {
              return docs.map(doc => {
                  const data = doc.payload.doc.data() as Chat;
                  data.id = doc.payload.doc.id;
                  data.currentUser = this.cs.http.auth.userLoggedin.username;
                  return data;
              })
          }))
      });

    }

    selectChat(chatid) {
        this.selectedChat = chatid;
    }

    createChat(receiver) {
        this.cs.createChat(this.cs.http.auth.userLoggedin.username, receiver)
        $('#addNewChatModal').modal('hide');
    }

}
