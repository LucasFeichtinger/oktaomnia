import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { HttpService } from '../services/http.service';
import { Observable, of, EMPTY } from 'rxjs';
import { take, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatResolverService implements Resolve<Object> {

  constructor(private http:HttpService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Object> | Observable<never> {
    this.http.data.loading = true;

    if (this.http.auth.userLoggedin === null || undefined) {
        let token = localStorage.getItem('token')

        return this.http.postLoginToken(token).pipe(
            take(1),
            mergeMap(user => {
                if (user) {
                    this.http.data.loading = false;
                    return of(user)
                } else {
                    this.http.data.loading = false;
                    return EMPTY
                }
            })
        )
    } else {
        this.http.data.loading = false;
        return of(this.http.auth.userLoggedin)
    }
  }
}
