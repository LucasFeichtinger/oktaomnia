import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { ChatService } from 'src/app/services/chat.service';
import { map } from 'rxjs/operators';
import { Message } from '../chat-list/chat-list.component';
import { WebSocketService } from 'src/app/services/web-socket.service';
declare var $: any;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnChanges {

    @Input()chatId

    messageCol:AngularFirestoreCollection<Message> = new AngularFirestoreCollection<Message>(null, null, null);
    messages:Observable<Message[]>;

    cookieUsername:string;

    message:string;
    currentChatPartner:string;

    chat;
  constructor(private cs: ChatService, private socketService:WebSocketService) { }

  ngOnChanges() {
      if (this.chatId) {
        this.getMessageToChat(this.chatId)
        this.cs.get(this.chatId).subscribe(data => {
            if (data.data.user1 === this.cs.http.auth.userLoggedin.username) {
                this.currentChatPartner = data.data.user2;
            } else {
                this.currentChatPartner = data.data.user1;
            }
        })
      }
    }

  getMessageToChat(chatId) {
    this.messages = null
    if (chatId !== undefined) {
        this.messageCol = this.cs.afs.collection('chats').doc(chatId).collection('messages');
        this.messages = this.messageCol.valueChanges();
        this.scrollBot()
        this.sortMessagesByDate()
    }
  }

  sortMessagesByDate() {
        this.messages = this.messages.pipe(map((data) => {
        if (data !== null || data !== undefined) {
            data.sort((a, b) => {
                a.msg.replace(/(.{25})/g, "$1\A");
                a.sendetAt = new Date(a.sendetAt).toString();
                b.sendetAt = new Date(b.sendetAt).toString();
                if (new Date(a.sendetAt) > new Date(b.sendetAt)) return 1;
                else if (new Date(b.sendetAt) > new Date(a.sendetAt)) return -1;
                return 0;
            });
        }
      return data;
    }));
  }

  sendMessage() {
      if (this.cs.http.auth.userLoggedin) {
        this.socketService.emitOnMessageSent(this.message, this.currentChatPartner, "message");
        const sendMess = this.message
        this.message = null;
        const ref = this.cs.afs.collection('chats').doc(this.chatId).collection('messages');
        this.scrollBot()
        return ref.add({ msg: sendMess, sendetAt: Date.now(), sender: this.cs.http.auth.userLoggedin.username })
    }
  }

  formatDate(date:string) {
    var xd = new Date(date).toString();
    xd = xd.split(" ")[4].split(":")[0] + ":" + xd.split(" ")[4].split(":")[1];
    return xd;
    }

    scrollBot() {
        //$('#scrollbot').scrollBot($('#scrollbot')[0].scrollHeight);
        /*
        $('#scrollbot').stop().animate({
          scrollTop: $('#scrollbot')[0].scrollHeight
        }, 800);
        */
       var myVar = window.setInterval(function() {
            var elem = document.getElementById('scrollbot');
            elem.scrollTop = elem.scrollHeight;
       }, 1)

       setTimeout(() => {
        clearInterval(myVar)
       }, 1000)
      }
}
