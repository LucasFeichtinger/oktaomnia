import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../core/auth.guard';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';
import { environment } from 'src/environments/environment';
import { FormsModule } from '@angular/forms';
import { ChatListComponent } from './chat-list/chat-list.component';
import { ChatComponent } from './chat/chat.component';
import { ChatResolverService } from './chat-resolver.service';
import { ChatMobileComponent } from './chat-mobile/chat-mobile.component';

const routes:Routes = [
    {
        path: 'chat',
        component: ChatListComponent,
        canActivate: [AuthGuard],
        resolve: {
            user: ChatResolverService
        }
    },
    {
        path: 'chat/:id',
        component: ChatMobileComponent,
        canActivate: [AuthGuard]
    }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule
  ],
  declarations: [ChatListComponent, ChatComponent, ChatMobileComponent],
  providers: [ChatResolverService]
})
export class ChatModule { }
