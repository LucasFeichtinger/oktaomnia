import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DienstleistungenComponent } from './dienstleistungen/dienstleistungen.component';
import { DienstleistungenModule } from './dienstleistungen/dienstleistungen.module';
import { AccountModule } from './account/account.module';
import { AuthGuard } from './core/auth.guard';

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "dienstleistungen"
    },
    {
        path: "dienstleistungen",
        loadChildren: () => DienstleistungenModule,
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
