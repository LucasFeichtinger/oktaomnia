import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-anfrage',
    templateUrl: './anfrage.component.html',
    styleUrls: ['./anfrage.component.css']
})
export class AnfrageComponent implements OnInit {

    user:any;

    @Input() anfrage: any;

    constructor(private http: HttpService, private router: Router) {
    }

    ngOnInit() {
        this.resolveUser();
    }
    resolveUser() {
        this.http.getUserById(this.anfrage.user).subscribe((data) => this.user = data);
    }
    changeAnfrage(status) {
        if(this.anfrage.status == 4 && status == 3)
            this.anfrage.status = 5;
        else
            this.anfrage.status = status;
        this.http.anfrageChangeStatus(this.anfrage, status);
    }

}
