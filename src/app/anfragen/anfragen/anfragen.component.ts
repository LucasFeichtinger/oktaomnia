import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
    selector: 'app-anfragen',
    templateUrl: './anfragen.component.html',
    styleUrls: ['./anfragen.component.css']
})
export class AnfragenComponent implements OnChanges {

    anfragen = [];
    new = 0;

    @Input() dienstleistung: any;

    constructor(private http: HttpService) { }

    ngOnChanges() {
        this.http.getAnfragenPerDienstleistung(this.dienstleistung._id).subscribe(data => {
            this.anfragen = data;
            this.countNew();
        });
    }

    changeAnfrage(anfrage, status) {
        this.anfragen.forEach(element => {
            if (element._id === anfrage._id) {
                element.status = status
            }
        })
        this.http.anfrageChangeStatus(anfrage, status);
    }

    countNew() {
        for ( let a of this.anfragen) {
            if(a.status === 0) this.new++;
        }
    }
}
