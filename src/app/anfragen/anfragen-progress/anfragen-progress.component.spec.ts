import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnfragenProgressComponent } from './anfragen-progress.component';

describe('AnfragenProgressComponent', () => {
  let component: AnfragenProgressComponent;
  let fixture: ComponentFixture<AnfragenProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnfragenProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnfragenProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
