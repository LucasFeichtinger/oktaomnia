import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-anfragen-progress',
  templateUrl: './anfragen-progress.component.html',
  styleUrls: ['./anfragen-progress.component.css']
})
export class AnfragenProgressComponent implements OnInit {

  constructor(private http:HttpService) { }

    anfrage$;

    @Input() anfragenID;

  ngOnInit() {
      this.anfrage$ = this.http.getAnfragePerID(this.anfragenID);
  }

}
