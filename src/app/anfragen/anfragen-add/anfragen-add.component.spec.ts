import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnfragenAddComponent } from './anfragen-add.component';

describe('AnfragenAddComponent', () => {
  let component: AnfragenAddComponent;
  let fixture: ComponentFixture<AnfragenAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnfragenAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnfragenAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
