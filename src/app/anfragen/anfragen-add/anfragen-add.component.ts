import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';

@Component({
    selector: 'app-anfragen-add',
    templateUrl: './anfragen-add.component.html',
    styleUrls: ['./anfragen-add.component.scss']
})
export class AnfragenAddComponent implements OnInit {

    @Input() dienstleistung:any;

    addAnfrageStatus:boolean = null;

    constructor(public http:HttpService) {

    }

    ngOnInit() {
        this.http.data.anfrageAddLoaded = true;
    }

    submit(f16:NgForm) {
        this.http.data.anfrageAddLoaded = false;
        var anfrage = {
            text: f16.value.anfragenaddtext,
            date: f16.value.anfragenadddate,
            user: this.http.auth.userLoggedin._id,
            dienstleister: this.dienstleistung.username,
            anzeige: this.dienstleistung._id,
            status: 0
        }
        this.http.addDienstleistungsAnfrage(anfrage);
        this.http.socket.emitOnMessageSent(f16.value.anfragenaddtext, this.dienstleistung.username, 'anzeigen-anfrage');
    }
}
