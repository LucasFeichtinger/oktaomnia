import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnfragenManageComponent } from './anfragen-manage.component';

import { BewertungAddComponent } from '../../bewertung/bewertung-add/bewertung-add.component';

describe('AnfragenManageComponent', () => {
  let component: AnfragenManageComponent;
  let fixture: ComponentFixture<AnfragenManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnfragenManageComponent , BewertungAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnfragenManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
