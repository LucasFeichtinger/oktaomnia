import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
    selector: 'app-anfragen-manage',
    templateUrl: './anfragen-manage.component.html',
    styleUrls: ['./anfragen-manage.component.css']
})
export class AnfragenManageComponent implements OnInit {

    anfragen = [];

    constructor(private http: HttpService) { }

    ngOnInit() {
        if (this.http.auth.userLoggedin)
            this.http.getAnfragenPerUser(this.http.auth.userLoggedin._id).subscribe((data)=> {
                // this.anfragen = data;
                // for(var i = data.length-1; i >= 0; i--) {
                //     this.anfragen.push(data[i]);
                // }
                this.anfragen = data.reverse();
                console.log(this.anfragen)
            });
    }

    changeAnfrage(anfrage, status) {
        var newanfragen = [];
        for (let element of this.anfragen) {
            if (element._id === anfrage._id) {
                element.status = status
                newanfragen.push(element);
            } else {
                newanfragen.push(element);
            }
        }
        this.anfragen = [];
        this.anfragen = newanfragen;
        this.http.anfrageChangeStatus(anfrage, status)
    }
}
