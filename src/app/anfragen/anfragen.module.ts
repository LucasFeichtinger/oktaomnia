import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnfragenComponent } from './anfragen/anfragen.component';
import { AnfragenAddComponent } from './anfragen-add/anfragen-add.component';
import { AnfragenManageComponent } from './anfragen-manage/anfragen-manage.component';
import { FormsModule } from '@angular/forms';
import { AnfragenProgressComponent } from './anfragen-progress/anfragen-progress.component';
import { BewertungModule } from '../bewertung/bewertung.module';
import { AnfrageComponent } from './anfrage/anfrage.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BewertungModule,
    RouterModule
  ],
  declarations: [AnfragenComponent, AnfragenAddComponent, AnfragenManageComponent, AnfragenProgressComponent, AnfrageComponent],
  exports: [AnfragenComponent, AnfragenAddComponent, AnfragenManageComponent, AnfragenProgressComponent]
})
export class AnfragenModule { }
