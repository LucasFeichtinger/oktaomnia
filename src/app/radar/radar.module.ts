import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RadarComponent } from './radar/radar.component';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from '../core/auth.guard';

const route:Routes = [
    {
        path: 'radar',
        component: RadarComponent,
        canActivate: [AuthGuard]
    }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(route),
    FormsModule,
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyA-1tLN8-pI_VtFj7L1D0n3P-aRX9IjLko' })
    // 'AIzaSyDu1cn4POm0ZagsTIEASmkRZ95qeqNkceI'
  ],
  declarations: [RadarComponent],
  providers: [GoogleMapsAPIWrapper],
})
export class RadarModule { }
