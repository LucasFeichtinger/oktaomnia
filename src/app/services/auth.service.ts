import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    userLoggedin = null;
    loggedin = false || localStorage.getItem('loggedin');
    token = null || localStorage.getItem('token');
    stayLoggedIn = false || localStorage.getItem('stayLoggedIn')

  constructor() { }

    get user() {
        return this.userLoggedin;
    }

    set user(user: User) {
        this.userLoggedin = user
    }

    isLoggedin(): boolean {
        if (this.loggedin === "true") {
            return true;
        }
        return false;
    }
}
