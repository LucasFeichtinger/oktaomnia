import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { HttpService } from './http.service';
import { map, combineLatest, switchMap } from 'rxjs/operators';
import { firestore } from 'firebase';
import { Observable, of } from 'rxjs';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Chat } from '../chat/chat-list/chat-list.component';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(public afs: AngularFirestore, public http: HttpService) { }

  async createChat(sender: string, receiver: string) {
    this.http.getUser(receiver).subscribe(element => {
        if (element) {
            this.afs.collection<any>('chats').add({ user1: sender, user2: receiver });
        }
    }, err => { console.log(err); });
  }

  get(chatId) {
      return this.afs
        .collection<any>('chats')
        .doc(chatId)
        .snapshotChanges()
        .pipe(
            map(doc => {
                return { id: doc.payload.id, data: doc.payload.data() as Chat };
            })
        )
  }

  getMessages(chatId) {
    return this.afs.collection<Message>('chats')
        .doc(chatId).collection('messages').valueChanges()
  }

  getAllChatsPerUser() {
        const user1Ref = this.afs.collection<any>('chats', ref => ref.where('user1', '==', this.http.auth.userLoggedin.username));
        const user2Ref = this.afs.collection<any>('chats', ref => ref.where('user2', '==', this.http.auth.userLoggedin.username));

        let combined = combineLatest(user1Ref.valueChanges(), user2Ref.valueChanges())
        return combined
    }

}
