import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import * as io from 'socket.io-client';
import { AuthService } from './auth.service';
import { UtilService } from './util.service';


@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

   private socket: SocketIOClient.Socket;

  constructor(private auth:AuthService, private util:UtilService) {
      this.socket = io('http://10.0.0.8:2000');
   }

   emitOnMessageSent(messageAdded, receiver, type) {
    this.socket.emit('messageAdded',
        messageAdded + "//20//"
        + this.auth.userLoggedin.username
        + "//20//" + receiver + "//20//" + type);
    // this.http.util.showInfo("Message send")
    // this.afs.collection("notifications").add({ title: "Neue Nachricht!",
    // description: "Neue Nachricht von " + this.dataService.getUser().username,
    // read: false, user: reciever, time: new Date(Date.now()).toJSON() });
   }

   consumeEventOnMessageSent(notificationAdded, currentUser) {
    this.socket.on('messageAdded', (notification:string) => {

        var trenn = notification.split("//20//");

        var notifi = trenn[0];
        var sender = trenn[1];
        var reciever = trenn[2];
        var type = trenn[3];

        if (type === "message" && currentUser === reciever) {
          this.util.showInfo("Neue Nachricht von " + sender + ": " + notifi)
        } else if (type === "anzeigen-anfrage" && currentUser === reciever) {
          this.util.showInfo("Anfrage von " + sender + " zu einer ihrer Dienstleistungen hinzugefügt: " + notifi)
          //this.afs.collection("notifications").add({ title: "Neue Anfrage!", description: "Neue Anfrage von " + sender, read: false, user: currentUser, time: new Date(Date.now()).toJSON() });
        }
      });
   }
}
