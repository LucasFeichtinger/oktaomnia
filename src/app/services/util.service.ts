import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(public data:DataService, private toastr:ToastrManager) { }

  tagAdd(tag) {
    this.data.tags.push(tag);
  }

  tagDelete(tag) {
    this.data.tags.splice(this.data.tags.indexOf(tag), 1);
  }

  showInfo(message: string) {
    this.toastr.customToastr(
        "<span style='color: #364462; font-size: 16px; text-align: center;'>" + message + "</span>",
        null,
        { enableHTML: true, position: 'bottom-right' }
        );
  }
}
