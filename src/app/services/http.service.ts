import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Dienstleistung } from '../models/dienstleistung';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { Category } from '../models/category';
import { DataService } from './data.service';
import { Router } from '@angular/router';
import { Anfrage } from '../models/anfrage';
import { AuthService } from './auth.service';
import { Bewertung } from '../models/bewertung';
import { UtilService } from './util.service';
import { WebSocketService } from './web-socket.service';

declare var $: any;

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(private http: HttpClient, public data: DataService,
        public auth: AuthService, private route: Router,
        public util: UtilService, public socket:WebSocketService) { }

    ip: string = "10.0.0.8";
    port: string = "2000";

    getDienstleistungen(): Observable<Dienstleistung> {
        return this.http.get<Dienstleistung>("http://" + this.ip + ":" + this.port + "/api/anzeigen");
    }
    getDienstleistungenPagination(number, limit): Observable<any> {
        let params = new HttpParams().set("number", number).set("limit", limit); // Create new HttpParams
        return this.http.get<any>("http://" + this.ip + ":" + this.port + "/api/anzeigen", {params: params});
    }

    getUsers(): Observable<User> {
        return this.http.get<User>("http://" + this.ip + ":" + this.port + "/api/user");
    }

    getDienstleistung(id): Observable<Dienstleistung> {
        return this.http.post<Dienstleistung>("http://" + this.ip + ":" + this.port + "/api/searchAnzeigen", { _id: id });
    }

    getUser(username): Observable<User> {
        return this.http.post<User>("http://" + this.ip + ":" + this.port + "/api/searchUser", { username: username });
    }

    getUserById(userid): Observable<User> {
        return this.http.post<User>("http://" + this.ip + ":" + this.port + "/api/Users", { userid: userid });
    }

    getCategories(): Observable<Category> {
        return this.http.get<Category>("http://" + this.ip + ":" + this.port + "/api/categories");
    }

    postLogin(username, password, stayLoggedIn) {
        // any nur weil das user objekt keinen token beinhaltet
        this.data.loadingButton = true;
        return this.http.post<any>("http://" + this.ip + ":" + this.port + "/api/login", { username: username, password: password })
            .subscribe(data => {
                this.auth.userLoggedin = data;
                this.auth.loggedin = 'true';
                localStorage.setItem('token', data.token);
                if (stayLoggedIn) localStorage.setItem('stayLoggedIn', 'true')
                $('#exampleModal').modal('hide');
                this.data.loadingButton = false;
                this.util.showInfo("Erfolgreich eingeloggt als " + data.username)
                this.socket.consumeEventOnMessageSent('messageAdded', this.auth.userLoggedin.username);
            }, err => {this.data.loginError = err; this.data.loadingButton = false;} );
    }

    postLoginToken(token) {
        return this.http.post<User>("http://" + this.ip + ":" + this.port + "/api/login", { token: token })
    }

    postLoginTokenForGuard() {
        return this.http.post<User>("http://" + this.ip + ":" + this.port + "/api/login", { token: this.auth.token })
    }

    postRegister(username, password, email, firstname, lastname, address, country, plz, dienstleister, addressDienstleister, countryDienstleister, plzDienstleister) {
        this.data.loadingButton = true;
        return this.http.post<User>("http://" + this.ip + ":" + this.port + "/api/register", {
            username: username,
            password: password,
            email: email,
            firstname: firstname,
            lastname: lastname,
            address: address,
            land: country,
            plz: plz,
            dienstleister: dienstleister,
            addressDienstleister: addressDienstleister,
            landDienstleister: countryDienstleister,
            plzDienstleister: plzDienstleister
        }).subscribe(() => { $('#exampleModal').modal('hide'); this.data.loadingButton = false; });
    }

    postAddDienstleistung(title, short_description, long_description, pph, category, tags) {
        this.data.loadingButton = true;
        return this.http.post<Dienstleistung>("http://" + this.ip + ":" + this.port + "/api/anzeigen", {
            title: title,
            short_description: short_description,
            long_description: long_description,
            username: this.auth.userLoggedin.username,
            pph: pph,
            category: category,
            tags: tags
        }).subscribe(data => { this.route.navigate(["/dienstleistungen"]); this.data.tags = []; this.data.loadingButton = false; this.util.showInfo("Dienstleistung " + data.title + " erfolgreich erstellt!") })
    }

    postEditDienstleistung(id, title, short_description, long_description, pph, category, tags) {
        this.data.loadingButton = true;
        //console.log(id);
        return this.http.post("http://" + this.ip + ":" + this.port + "/api/updateAnzeigen", {
            toUpdate: id,
            title: title,
            short_description: short_description,
            long_description: long_description,
            username: this.auth.userLoggedin.username,
            pph: pph,
            category: category,
            tags: tags
        }).subscribe(data => {
            this.util.showInfo("Dienstleistung erfolgreich geändert")
            this.data.tags = [];
            this.route.navigate(["/dienstleistungen"]);
            this.data.loadingButton = false;
        });
    }

    getAnzeigenPerUser(username) {
        return this.http.post<Dienstleistung[]>("http://" + this.ip + ":" + this.port + "/api/getAnzeigenPerUser", { username: username });
    }

    getAnfragenPerUser(userid): Observable<Anfrage[]> {
        return this.http.post<Anfrage[]>("http://" + this.ip + ":" + this.port + "/api/getAnzeigenAnfragen", { userid: userid });
    }

    getAnfragenPerDienstleistung(dienstleistungId): Observable<Anfrage[]> {
        return this.http.post<Anfrage[]>("http://" + this.ip + ":" + this.port + "/api/getAnzeigenAnfragen", { anzeigeid: dienstleistungId });
    }

    getAnfragePerID(anfragenId: Observable<Anfrage>) {
        return this.http.post<Anfrage>("http://" + this.ip + ":" + this.port + "/api/getAnzeigenAnfragen", { anfrageid: anfragenId })
    }

    addDienstleistungsAnfrage(anfrage) {
        return this.http.post<any>("http://" + this.ip + ":" + this.port + "/api/addAnzeigenAnfrage", { anfrage }).subscribe(data => {
            $('.circle-loader').toggleClass('load-complete');
            $('.checkmark').toggle();
            setTimeout(() => this.data.anfrageAddLoaded = true, 2000);
        });
    }

    addBewertung(bewertung: any) {
        return this.http.post<any>("http://" + this.ip + ":" + this.port + "/api/addBewertung", { bewertung: bewertung })
    }

    loadBewertungenByUser(user: User): Observable<Bewertung[]> {
        return this.http.post<any>("http://" + this.ip + ":" + this.port + "/api/getBewertungen", { user: user })
    }

    // Dont touch this shit || dont know why, it works only this way || dont know, dont care
    anfrageChangeStatus(anfrage, statuss) {
        return this.http.post("http://" + this.ip + ":" + this.port + "/api/AnfrageChangeStatus", { AnfrageId: anfrage._id, status: statuss }).subscribe(data => {
        }, err => { console.log(err) });
    }

    checkIfUserNameExists(p_username: string) {
        var username = ""
        this.getUser(p_username).subscribe(element => { username = element.username })

        setTimeout(() => {
            if (p_username.toLowerCase() === username.toLowerCase()) {
                this.data.usernameTaken = true
            } else {
                this.data.usernameTaken = false
            }
        }, 2000)
    }

    getAllDienstleister() {
        return this.http.get("http://" + this.ip + ":" + this.port + "/api/dienstleister");
    }

    textSearch(query, category) {
        return this.http.post("http://" + this.ip + ":" + this.port + "/api/textsearch", { query: query, category: category })
    }
}
