import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

    loggedin = false || localStorage.getItem('loggedin');
    token = null || localStorage.getItem('token');
    userLoggedin = null;
    tags = [];
    loginError = null;
    anfrageAddLoaded = true;
    usernameTaken = false;
    loading:boolean = false;
    loadingButton:boolean = false;

  constructor() { }
}
