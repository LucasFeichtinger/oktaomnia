import { Component, OnInit } from '@angular/core';
import { HttpService } from './services/http.service';
import { NgForm } from '@angular/forms';
import { WebSocketService } from './services/web-socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(public http:HttpService, public socket:WebSocketService) { }

  ngOnInit() {
    if (this.http.auth.loggedin == "false") {
        this.http.postLogin(localStorage.getItem('username'), localStorage.getItem('password'), true);
    } else if (this.http.auth.token != null) {
        if (this.http.auth.stayLoggedIn === 'true') {
            this.http.data.loading = true;
            this.http.postLoginToken(this.http.auth.token).subscribe(data => {
                this.http.util.showInfo("Du bist erfolgreich eingeloggt als " + data.username);
                this.http.data.loading = false;
                this.http.auth.userLoggedin = data;
                this.http.auth.loggedin = "true";
                this.socket.consumeEventOnMessageSent('messageAdded', this.http.auth.userLoggedin.username)
            });
        } else {
            localStorage.removeItem('token')
        }
    }
  }

    onSubmit(f1: NgForm) {
        this.http.postLogin(f1.value.username, f1.value.password, f1.value.stayLoggedIn);
    }

    onSubmitRegister(f1: NgForm) {
        //this.http.postRegister(f1.value.username, f1.value.password, f1.value.email, f1.value.firstname, f1.value.lastname, f1.value.country, f1.value.plz);
    }

    logout() {
        this.http.auth.loggedin = 'false';
        this.http.auth.userLoggedin = null;
        localStorage.removeItem('token');
        localStorage.removeItem('stayLoggedIn')
    }
}
