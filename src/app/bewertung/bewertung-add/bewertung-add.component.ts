import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Anfrage } from 'src/app/models/anfrage';
import { NgForm } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';


@Component({
    selector: 'app-bewertung-add',
    templateUrl: './bewertung-add.component.html',
    styleUrls: ['./bewertung-add.component.css']
})
export class BewertungAddComponent implements OnInit {
    @ViewChild("starrating") starrating: ElementRef;

    rating: number

    @Input() anfrage;

    constructor(private http: HttpService) { }

    ngOnInit() {
        this.changerating(3);
        console.log(this.anfrage);
    }

    onSubmit(data: NgForm) {
        var bewertung = {
            starrating: this.rating,
            langBewertung: data.value.textbewertung,
            anfrageId: this.anfrage._id,
            userId: this.anfrage.user,
            dienstleister: this.anfrage.dienstleister
        };
        this.http.addBewertung(bewertung).subscribe( data => {
            this.anfrage.status = 6;
            console.log("finished");
        });
        this.http.anfrageChangeStatus(this.anfrage, 6);
    }
    changerating(num:number) {
        this.rating = num;
        var count = 0
        this.starrating.nativeElement.childNodes[count].childNodes[0].style.color = "yellow";
        while ((num-1) >= count) {
            this.starrating.nativeElement.childNodes[count].childNodes[0].style.color = "yellow";
            count++;
        }
        count = 4;
        while (num <= count) {
            this.starrating.nativeElement.childNodes[count].childNodes[0].style.color = "black";
            count--;
        }
    }
}
