import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BewertungAddComponent } from '../../bewertung/bewertung-add/bewertung-add.component';

describe('BewertungAddComponent', () => {
  let component: BewertungAddComponent;
  let fixture: ComponentFixture<BewertungAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BewertungAddComponent , BewertungAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BewertungAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
