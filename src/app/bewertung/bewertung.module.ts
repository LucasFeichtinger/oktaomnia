import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BewertungAddComponent } from './bewertung-add/bewertung-add.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [BewertungAddComponent],
  exports: [BewertungAddComponent]
})
export class BewertungModule { }
