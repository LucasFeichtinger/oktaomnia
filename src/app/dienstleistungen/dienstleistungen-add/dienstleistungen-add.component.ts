import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { UtilService } from 'src/app/services/util.service';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-dienstleistungen-add',
    templateUrl: './dienstleistungen-add.component.html',
    styleUrls: ['./dienstleistungen-add.component.css']
})
export class DienstleistungenAddComponent implements OnInit {

    categories;
    tagNotAdded;

    kuCharCounter;
    laCharCounter;

    constructor(public http: HttpService, private util: UtilService) { }

    ngOnInit() {
        this.categories = this.http.getCategories();
    }

    tagAdd() {
        this.util.tagAdd(this.tagNotAdded);
        this.tagNotAdded = "";
    }

    tagDelete(tag) {
        this.util.tagDelete(tag)
    }

    onSubmit(f1: NgForm) {
        this.http.postAddDienstleistung(f1.value.inputTitel, f1.value.inputShortDesc, f1.value.inputLongDesc, f1.value.inputPph, f1.value.inputCategory, this.util.data.tags);
    }

}
