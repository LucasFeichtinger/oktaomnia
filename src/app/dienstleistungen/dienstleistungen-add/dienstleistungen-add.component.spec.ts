import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DienstleistungenAddComponent } from './dienstleistungen-add.component';

describe('DienstleistungenAddComponent', () => {
  let component: DienstleistungenAddComponent;
  let fixture: ComponentFixture<DienstleistungenAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DienstleistungenAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DienstleistungenAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
