import { TestBed, inject } from '@angular/core/testing';

import { DienstleistungEditResolverService } from './dienstleistung-edit-resolver.service';

describe('DienstleistungEditResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DienstleistungEditResolverService]
    });
  });

  it('should be created', inject([DienstleistungEditResolverService], (service: DienstleistungEditResolverService) => {
    expect(service).toBeTruthy();
  }));
});
