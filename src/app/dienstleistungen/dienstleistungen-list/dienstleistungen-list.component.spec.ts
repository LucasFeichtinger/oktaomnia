import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DienstleistungenListComponent } from './dienstleistungen-list.component';

describe('DienstleistungenListComponent', () => {
  let component: DienstleistungenListComponent;
  let fixture: ComponentFixture<DienstleistungenListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DienstleistungenListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DienstleistungenListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
