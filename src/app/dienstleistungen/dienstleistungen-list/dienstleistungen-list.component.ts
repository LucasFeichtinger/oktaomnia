import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-dienstleistungen-list',
  templateUrl: './dienstleistungen-list.component.html',
  styleUrls: ['./dienstleistungen-list.component.css']
})
export class DienstleistungenListComponent implements OnInit {
    dienstleistung$;
    user$;
    dienstleistungenPerUser;
    loading;

    @Input() username:string;

  constructor(private route:ActivatedRoute, private http:HttpService) {

  }

  ngOnInit() {
    this.loading = true;
    this.http.getAnzeigenPerUser(this.username).subscribe(data => {
        this.dienstleistungenPerUser = data;
        this.loading = false;
    })
  }
}
