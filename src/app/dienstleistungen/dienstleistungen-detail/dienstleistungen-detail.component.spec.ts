import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DienstleistungenDetailComponent } from './dienstleistungen-detail.component';

describe('DienstleistungenDetailComponent', () => {
  let component: DienstleistungenDetailComponent;
  let fixture: ComponentFixture<DienstleistungenDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DienstleistungenDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DienstleistungenDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
