import { Component, OnInit, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { Dienstleistung } from 'src/app/models/dienstleistung';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-dienstleistungen-detail',
  templateUrl: './dienstleistungen-detail.component.html',
  styleUrls: ['./dienstleistungen-detail.component.css']
})
export class DienstleistungenDetailComponent implements OnInit {

    dienstleistung$;
    user$;
    dienstleistungenPerUser;

  constructor(private route:ActivatedRoute, private http:HttpService) {
    this.route.params.subscribe(params => this.dienstleistung$ = params.id);
  }

  ngOnInit() {
    //this.dienstleistung$ = this.http.getDienstleistung(this.dienstleistung$).subscribe(data => {
    //    this.dienstleistung$ = data;
    //    this.http.getUser(data.username).subscribe(e => this.user$ = e);
    //    this.getDienstleistungenPerUser(data.username);
    //});
    this.route.data.subscribe((data: { dienstleistung, user, }) => {
        this.dienstleistung$ = data.dienstleistung.dienstleistung;
        this.user$ = data.dienstleistung.user;
        //this.http.getUser(data.dienstleistung.username).subscribe(e => this.user$ = e)
        this.getDienstleistungenPerUser(data.dienstleistung.dienstleistung.username)

    })

    //this.dienstleistung$ = this.route.snapshot.data['dienstleistung']
    //this.user$ = this.route.snapshot.data['user']
    //this.getDienstleistungenPerUser(this.dienstleistung$.username)
  }

  changeDienstleistung(id) {
    this.dienstleistung$ = this.http.getDienstleistung(id).subscribe(data => {
        this.dienstleistung$ = data;
    });
  }

  getDienstleistungenPerUser(username) {
    this.dienstleistungenPerUser = this.http.getAnzeigenPerUser(username);
  }
}
