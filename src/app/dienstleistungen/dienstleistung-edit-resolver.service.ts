import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { HttpService } from '../services/http.service';
import { mergeMap, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DienstleistungEditResolverService implements Resolve<Object> {

  constructor(private http:HttpService, private route:Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Object> | Observable<never> {
    this.http.data.loading = true;
    let id = route.paramMap.get('id');
    return this.http.getDienstleistung(id).pipe(
        take(1),
        mergeMap(dienstleistung => {
            if (dienstleistung) {
                this.http.data.loading = false;
                return of(dienstleistung)
            } else {
                this.http.data.loading = false;
                this.route.navigate(['/dienstleistungen'])
                return EMPTY
            }
        })
    )
  }
}
