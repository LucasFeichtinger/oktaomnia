import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-dienstleistungen',
    templateUrl: './dienstleistungen.component.html',
    styleUrls: ['./dienstleistungen.component.css']
})
export class DienstleistungenComponent implements OnInit {

    countPerPage = 10;
    currentPage = 1;
    maxPage;

    dienstleistungen;
    categories;
    limit;
    searchString;
    searchCategory;

    constructor(private http: HttpService) {
        this.http.data.loading = true;
        this.http.getDienstleistungenPagination(0, this.countPerPage).subscribe(data => {
            console.log(data);
            // pages berechnen und kommazahl mit Math.ceil() aufrunden
            this.maxPage = Math.ceil(data.count/this.countPerPage);
            this.dienstleistungen = data.values;
            this.http.data.loading = false;
        });
        this.categories = this.http.getCategories();

    }

    array(number) {
        var arr = Array(number);
        for (var i = 1; i <= number; i++) {
            arr[i-1] = i;
        }
        return arr;
    }

    loadPage(pagenum) {
        this.currentPage = pagenum;
        var itemindex = (pagenum * this.countPerPage) - this.countPerPage;

        this.http.data.loading = true;
        this.http.getDienstleistungenPagination(itemindex, this.countPerPage).subscribe(data => {
            // pages berechnen und kommazahl mit Math.ceil() aufrunden
            this.maxPage = Math.ceil(data.count/this.countPerPage);
            this.dienstleistungen = data.values;
            this.http.data.loading = false;
        });
    }

    changeCountPerPage(count) {
        if(this.countPerPage < count) {
            var factor = this.countPerPage/count;
        } else {
            var factor = count/this.countPerPage;
        }

        this.countPerPage = count;
        this.loadPage(Math.ceil(this.currentPage*factor));
    }

    ngOnInit() {

    }

    searchDienstleistung(title, category) {
        if (title === undefined) title = ""
        if (category === undefined || category === "kbk") category = null
        this.http.data.loading = true;
        this.http.textSearch(title, category).subscribe(data => {
            this.dienstleistungen = data;
            this.http.data.loading = false;
        }, err => {
            this.http.data.loading = false;
        })
    }
}
