import { TestBed, inject } from '@angular/core/testing';

import { DienstleistungDetailResolverService } from './dienstleistung-detail-resolver.service';

describe('DienstleistungDetailResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DienstleistungDetailResolverService]
    });
  });

  it('should be created', inject([DienstleistungDetailResolverService], (service: DienstleistungDetailResolverService) => {
    expect(service).toBeTruthy();
  }));
});
