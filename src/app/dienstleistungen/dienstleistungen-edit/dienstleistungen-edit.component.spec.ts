import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DienstleistungenEditComponent } from './dienstleistungen-edit.component';

describe('DienstleistungenEditComponent', () => {
  let component: DienstleistungenEditComponent;
  let fixture: ComponentFixture<DienstleistungenEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DienstleistungenEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DienstleistungenEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
