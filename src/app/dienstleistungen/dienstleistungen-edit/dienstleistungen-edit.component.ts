import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-dienstleistungen-edit',
  templateUrl: './dienstleistungen-edit.component.html',
  styleUrls: ['./dienstleistungen-edit.component.css']
})
export class DienstleistungenEditComponent implements OnInit {

    dienstleistung;
    form:FormGroup;
    categories;
    tagNotAdded;

  constructor(private route:ActivatedRoute, public http:HttpService, private formBuilder:FormBuilder, private util:UtilService) {
    this.route.params.subscribe(params => this.dienstleistung = params.id);
   }

  ngOnInit() {
    this.categories = this.http.getCategories();
    /*
    this.http.getDienstleistung(this.dienstleistung).subscribe(data => {
        this.dienstleistung = data;
        this.http.data.tags = data.tags;
        this.form = new FormGroup({
            title: new FormControl(data.title),
            short_description: new FormControl(data.short_description),
            long_description: new FormControl(data.long_description),
            pph: new FormControl(data.pph),
            category: new FormControl(data.category)
        });
    });
    */
    this.route.data.subscribe((data : { dienstleistung }) => {
        this.dienstleistung = data.dienstleistung;
        this.http.data.tags = data.dienstleistung.tags;
        this.form = new FormGroup({
            title: new FormControl(data.dienstleistung.title),
            short_description: new FormControl(data.dienstleistung.short_description),
            long_description: new FormControl(data.dienstleistung.long_description),
            pph: new FormControl(data.dienstleistung.pph),
            category: new FormControl(data.dienstleistung.category)
        });
    })
  }

  onSubmit() {
    //console.log(this.dienstleistung._id, this.form.controls['title'].value, this.form.controls['short_description'].value, this.form.controls['long_description'].value, this.form.controls['pph'].value, this.form.controls['category'].value, this.http.data.tags);
    this.http.postEditDienstleistung(this.dienstleistung._id, this.form.controls['title'].value, this.form.controls['short_description'].value, this.form.controls['long_description'].value, this.form.controls['pph'].value, this.form.controls['category'].value, this.http.data.tags);
  }


  tagAdd() {
    this.util.tagAdd(this.tagNotAdded);
    this.tagNotAdded = "";
  }

  tagDelete(tag) {
    this.util.tagDelete(tag)
  }

}
