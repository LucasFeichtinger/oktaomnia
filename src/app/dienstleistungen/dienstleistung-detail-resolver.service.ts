import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Dienstleistung } from '../models/dienstleistung';
import { Observable, of, EMPTY } from 'rxjs';
import { HttpService } from '../services/http.service';
import { take, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DienstleistungDetailResolverService implements Resolve<Object> {

  constructor(private route:Router, private http:HttpService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Object> | Observable<never> {
    this.http.data.loading = true;
    let id = route.paramMap.get('id');
    return this.http.getDienstleistung(id).pipe(
        take(1),
        mergeMap(dienstleistung => {
            if (dienstleistung) {
                this.http.data.loading = false;
                return this.http.getUser(dienstleistung.username).pipe(
                    take(1),
                    mergeMap(user => {
                        if (user) {
                            this.http.data.loading = false;
                            return of( { dienstleistung, user })
                        }
                    })
                )
            } else {
                this.http.data.loading = false;
                this.route.navigate(['/dienstleistungen'])
                return EMPTY
            }
        })
    )
  }
}
