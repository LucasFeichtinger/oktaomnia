import { DienstleistungenModule } from './dienstleistungen.module';

describe('DienstleistungenModule', () => {
  let dienstleistungenModule: DienstleistungenModule;

  beforeEach(() => {
    dienstleistungenModule = new DienstleistungenModule();
  });

  it('should create an instance', () => {
    expect(dienstleistungenModule).toBeTruthy();
  });
});
