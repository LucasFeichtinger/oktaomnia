import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DienstleistungenEditComponent } from './dienstleistungen-edit/dienstleistungen-edit.component';
import { DienstleistungenDetailComponent } from './dienstleistungen-detail/dienstleistungen-detail.component';
import { DienstleistungenAddComponent } from './dienstleistungen-add/dienstleistungen-add.component';
import { Routes, RouterModule } from '@angular/router';
import { DienstleistungenComponent } from './dienstleistungen.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AnfragenModule } from '../anfragen/anfragen.module';
import { AuthGuard } from '../core/auth.guard';
import { DienstleistungDetailResolverService } from './dienstleistung-detail-resolver.service';
import { DienstleistungenListComponent } from './dienstleistungen-list/dienstleistungen-list.component';
import { DienstleistungEditResolverService } from './dienstleistung-edit-resolver.service';
const routes: Routes = [
    {
        path: '',
        component: DienstleistungenComponent
    },
    {
        path: 'hinzufuegen',
        component: DienstleistungenAddComponent,
        canActivate: [AuthGuard]
    },
    {
        path: ':id/detail',
        component: DienstleistungenDetailComponent,
        resolve: {
            dienstleistung: DienstleistungDetailResolverService
        }
    },
    {
        path: ':id/edit',
        component: DienstleistungenEditComponent,
        canActivate: [AuthGuard],
        resolve: {
            dienstleistung: DienstleistungEditResolverService
        }
    }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    AnfragenModule
  ],
  declarations: [DienstleistungenComponent, DienstleistungenEditComponent, DienstleistungenDetailComponent, DienstleistungenAddComponent, DienstleistungenListComponent],
  exports: [DienstleistungenComponent, DienstleistungenEditComponent, DienstleistungenDetailComponent, DienstleistungenAddComponent, DienstleistungenListComponent],
  providers: [DienstleistungDetailResolverService]
})
export class DienstleistungenModule { }
