import { Category } from "./category";

export class Dienstleistung {
    _id: string;
    title: string;
    short_description: string;
    long_description: string;
    username: string;
    pph: number;
    category: Category;
    tags: Array<string>;
}
