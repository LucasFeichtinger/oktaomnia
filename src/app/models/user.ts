export class User {
    id: string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    status: string;
    email: string;
    country: string;
    plz: string;
    geometry: any;
    dienstleister: any;
}
