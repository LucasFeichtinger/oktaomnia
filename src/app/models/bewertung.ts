export class Bewertung {
    _id: string;
    starrating: number;
    langBewertung: string;
    anfrageId: string;
    userId: string;
    dienstleister: string;
}
