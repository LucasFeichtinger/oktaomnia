export class Anfrage {
    _id: string;
    text: string;
    date: Date
    user: string;
    dienstleister: string;
    anzeige: string;
    status: number;
    timestamp;
}
