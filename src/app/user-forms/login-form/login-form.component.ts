import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { NgForm, ValidationErrors } from '@angular/forms';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { map, take, debounceTime } from 'rxjs/operators';
import { element } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

    loginForm: FormGroup;

  constructor(public http:HttpService, private fb:FormBuilder) { }

  ngOnInit() {
  }

  onSubmit(f1: NgForm) {
    this.http.postLogin(f1.value.username, f1.value.password, f1.value.stayLoggedIn);
  }
}
