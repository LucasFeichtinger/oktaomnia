import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, AbstractControl, FormGroup, Validators } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { Observable } from 'rxjs';
import { of as observableOf } from 'rxjs';
import { map, delay, debounceTime } from 'rxjs/operators';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

    loginForm: FormGroup;

  constructor(public http:HttpService, private fb:FormBuilder) { }

  ngOnInit() {
    /*
    this.loginForm = this.fb.group({
          username: ['', [Validators.required, this.checkIfNameExists.bind(this)]],
          password: ['', Validators.required],
          confirmPassword: ['', Validators.required],
          email: ['', Validators.required],
          firstname: ['', Validators.required],
          lastname: ['', Validators.required],
          country: ['', Validators.required],
          plz: ['', Validators.required]
      })
      */
  }

  onSubmitRegister(f1: NgForm) {
    this.http.postRegister(f1.value.username, f1.value.password, f1.value.email, f1.value.firstname, f1.value.lastname, f1.value.address, f1.value.country, f1.value.plz, f1.value.dienstleister, f1.value.addressDienstleister, f1.value.countryDienstleister, f1.value.plzDienstleister);
  }

  get username() {
    return this.loginForm.get("username");
  }

  get password() {
    return this.loginForm.get("password");
  }

  checkIfNameExists(control: AbstractControl) {
    this.http.checkIfUserNameExists(control.value);
    setTimeout(() => {
        console.log(this.http.data.usernameTaken)
        return observableOf(this.http.data.usernameTaken).pipe(map(result => result ? { usernameTaken: true } : null))
    }, 2000)
  }
}
