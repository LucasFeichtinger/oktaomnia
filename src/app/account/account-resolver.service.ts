import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { HttpService } from '../services/http.service';
import { take, mergeMap } from 'rxjs/operators';
import { of, EMPTY, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountResolverService implements Resolve<Object> {

  constructor(private http:HttpService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Object> | Observable<never> {
    this.http.data.loading = true;
    let token = localStorage.getItem('token')

    return this.http.postLoginToken(token).pipe(
        take(1),
        mergeMap(user => {
            if (user) {
                this.http.data.loading = false;
                return of(user);
            } else {
                this.http.data.loading = false;
                return EMPTY
            }
        })
    )
  }
}
