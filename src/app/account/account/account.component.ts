import { Component, OnInit, OnChanges } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  constructor(private route:ActivatedRoute, private http: HttpService) { }

  anzeigenPerUser;
  selectedAnzeige;

  ngOnInit() {
    this.route.data.subscribe((data: { user, }) => {
        this.anzeigenPerUser = this.http.getAnzeigenPerUser(data.user.username);
    })
  }

  selectAnzeige(anzeige) {
    this.selectedAnzeige = anzeige;
  }

}
