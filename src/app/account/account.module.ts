import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account/account.component';
import { EinstellungenComponent } from './einstellungen/einstellungen.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AnfragenModule } from '../anfragen/anfragen.module';
import { AuthGuard } from '../core/auth.guard';
import { AccountResolverService } from './account-resolver.service';

const routes: Routes = [
    {
        path: 'account',
        component: AccountComponent,
        canActivate: [AuthGuard],
        resolve: {
            user: AccountResolverService
        }
    },
    {
        path: 'einstellungen',
        component: EinstellungenComponent,
        canActivate: [AuthGuard]
    }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes),
    AnfragenModule,
  ],
  declarations: [AccountComponent, EinstellungenComponent],
  exports: [AccountComponent, EinstellungenComponent],
  providers: [AccountResolverService]
})
export class AccountModule { }
